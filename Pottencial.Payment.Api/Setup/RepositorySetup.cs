﻿using Pottencial.Payment.Domain.Interfaces.Repositories;
using Pottencial.Payment.Infra.Repositories;
using System.Diagnostics.CodeAnalysis;

namespace Pottencial.Payment.Api.Setup
{
    /// <summary>
    /// Provides methods to set up repository dependencies.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class RepositorySetup
    {
        /// <summary>
        /// Adds the repository dependencies to the service collection.
        /// </summary>
        /// <param name="services">The service collection to add dependencies to.</param>
        public static void AddDependencyRepository(this IServiceCollection services)
        {
            ArgumentNullException.ThrowIfNull(services);
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ISellerRepository, SellerRepository>();
            services.AddScoped<ISaleRepository, SaleRepository>();
        }
    }
}