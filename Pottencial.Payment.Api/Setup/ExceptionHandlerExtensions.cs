﻿using Microsoft.AspNetCore.Diagnostics;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Domain.Exceptions;
using System.Net;

namespace Pottencial.Payment.Api.Setup
{
    /// <summary>
    /// Provides extension methods for handling exceptions in the application.
    /// </summary>
    public static class ExceptionHandlerExtensions
    {
        /// <summary>
        /// Configures the application to use a custom exception handler.
        /// </summary>
        /// <param name="app">The application builder.</param>
        public static void UseCustomExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(configure =>
            {
                configure.Run(async context =>
                {
                    IExceptionHandlerPathFeature? exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                    if (exceptionHandlerPathFeature?.Error is not null)
                    {
                        int statusCode = (int)HttpStatusCode.InternalServerError;
                        string errorMessage = exceptionHandlerPathFeature.Error.Message;
                        if (exceptionHandlerPathFeature?.Error is DomainException)
                        {
                            statusCode = (int)HttpStatusCode.BadRequest;
                        }
                        else if (exceptionHandlerPathFeature?.Error is NotFoundException)
                        {
                            statusCode = (int)HttpStatusCode.NotFound;
                        }
                        else
                        {
                            errorMessage = "Ocorreu um erro inesperado";
                        }
                        context.Response.StatusCode = statusCode;
                        await context.Response.WriteAsJsonAsync(new DefaultOutput(false, errorMessage));
                    }
                });
            });
        }
    }
}