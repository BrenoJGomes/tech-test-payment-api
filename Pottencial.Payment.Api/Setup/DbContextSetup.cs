﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Infra.Contexts;
using System.Diagnostics.CodeAnalysis;

namespace Pottencial.Payment.Api.Setup
{
    /// <summary>
    /// Provides methods to configure the database context for the application.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class DbContextSetup
    {
        /// <summary>
        /// Adds the SQL Server database context to the service collection.
        /// </summary>
        /// <param name="services">The service collection to add the context to.</param>
        /// <param name="configuration">The configuration containing the connection string.</param>
        public static void AddSqlServerDbContext(this IServiceCollection services, IConfiguration configuration)
        {
            ArgumentNullException.ThrowIfNull(services);
            ArgumentNullException.ThrowIfNull(configuration);
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(configuration.GetConnectionString("DefaultConnection")),
                ServiceLifetime.Scoped
            );
        }
    }
}
