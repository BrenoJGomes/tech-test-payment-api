﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Application.Handlers.Sales;
using Pottencial.Payment.Application.Handlers.Sellers;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Application.ViewModels.Sellers;
using System.Diagnostics.CodeAnalysis;

namespace Pottencial.Payment.Api.Setup
{
    /// <summary>
    /// Provides methods to register request handlers for product-related commands.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class HandlerSetup
    {
        /// <summary>
        /// Registers the request handlers for product-related commands.
        /// </summary>
        /// <param name="services">The service collection to add the handlers to.</param>
        public static void AddDependencyHandler(this IServiceCollection services)
        {
            ArgumentNullException.ThrowIfNull(services);

            // Products
            services.AddScoped<IRequestHandler<AddProductInput, DefaultOutput<ProductViewModel>>, AddProductHandler>();
            services.AddScoped<IRequestHandler<FindProductInput, DefaultOutput<ProductViewModel>>, FindProductHandler>();
            services.AddScoped<IRequestHandler<GetAllProductsInput, DefaultOutput<IEnumerable<ProductViewModel>>>, GetAllProductsHandler>();
            services.AddScoped<IRequestHandler<UpdateProductInput, DefaultOutput<ProductViewModel>>, UpdateProductHandler>();
            services.AddScoped<IRequestHandler<DeleteProductInput, DefaultOutput>, DeleteProductHandler>();

            // Sellers
            services.AddScoped<IRequestHandler<AddSellerInput, DefaultOutput<SellerViewModel>>, AddSellerHandler>();
            services.AddScoped<IRequestHandler<FindSellerInput, DefaultOutput<SellerViewModel>>, FindSellerHandler>();
            services.AddScoped<IRequestHandler<GetAllSellersInput, DefaultOutput<IEnumerable<SellerViewModel>>>, GetAllSellersHandler>();
            services.AddScoped<IRequestHandler<UpdateSellerInput, DefaultOutput<SellerViewModel>>, UpdateSellerHandler>();
            services.AddScoped<IRequestHandler<DeleteSellerInput, DefaultOutput>, DeleteSellerHandler>();

            // Sales
            services.AddScoped<IRequestHandler<AddSaleInput, DefaultOutput<SaleViewModel>>, AddSaleHandler>();
            services.AddScoped<IRequestHandler<FindSaleInput, DefaultOutput<SaleViewModel>>, FindSaleHandler>();
            services.AddScoped<IRequestHandler<GetAllSalesInput, DefaultOutput<IEnumerable<SaleViewModel>>>, GetAllSalesHandler>();
            services.AddScoped<IRequestHandler<UpdateSaleStatusInput, DefaultOutput<SaleViewModel>>, UpdateSaleStatusHandler>();
            services.AddScoped<IRequestHandler<UpdateSaleProductsInput, DefaultOutput<SaleViewModel>>, UpdateSaleProductsHandler>();
        }
    }
}
