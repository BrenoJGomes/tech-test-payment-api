﻿using System.Diagnostics.CodeAnalysis;

namespace Pottencial.Payment.Api.Setup
{
    /// <summary>
    /// Provides extension methods for setting up MediatR in the service collection.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public static class MediatRSetup
    {
        /// <summary>
        /// Adds MediatR services to the specified IServiceCollection.
        /// </summary>
        /// <param name="services">The service collection to add MediatR services to.</param>
        /// <exception cref="ArgumentNullException">Thrown when services is null.</exception>
        public static void AddMediatR(this IServiceCollection services)
        {
            ArgumentNullException.ThrowIfNull(services);
            services.AddMediatR(cfg =>
            {
                cfg.RegisterServicesFromAssembly(typeof(Program).Assembly);
            });
        }
    }
}
