﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sellers;

namespace Pottencial.Payment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SellerController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SellerController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<DefaultOutput<SellerViewModel>> Post([FromBody] AddSellerInput request)
        {
            return await _mediator.Send(request);
        }

        [HttpGet("{id}")]
        public async Task<DefaultOutput<SellerViewModel>> Get(Guid id)
        {
            return await _mediator.Send(new FindSellerInput() { Id = id});
        }

        [HttpGet]
        public async Task<DefaultOutput<IEnumerable<SellerViewModel>>> GetAll()
        {
            return await _mediator.Send(new GetAllSellersInput());
        }

        [HttpPut("{id}")]
        public async Task<DefaultOutput<SellerViewModel>> Put(Guid id, [FromBody] UpdateSellerInput input)
        {
            return await _mediator.Send(input);
        }

        [HttpDelete("{id}")]
        public async Task<DefaultOutput> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteSellerInput() { Id = id});
        }
    }
}
