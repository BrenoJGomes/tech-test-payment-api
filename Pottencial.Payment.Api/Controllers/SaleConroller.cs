﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sales;

namespace Pottencial.Payment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SaleController : ControllerBase
    {
        private readonly IMediator _mediator;

        public SaleController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<DefaultOutput<SaleViewModel>> Post([FromBody] AddSaleInput request)
        {
            return await _mediator.Send(request);
        }

        [HttpGet("{id}")]
        public async Task<DefaultOutput<SaleViewModel>> Get(Guid id)
        {
            return await _mediator.Send(new FindSaleInput() { Id = id });
        }

        [HttpGet]
        public async Task<DefaultOutput<IEnumerable<SaleViewModel>>> GetAll()
        {
            return await _mediator.Send(new GetAllSalesInput());
        }

        [HttpPatch("status/{id}")]
        public async Task<DefaultOutput<SaleViewModel>> Put(Guid id, [FromBody] UpdateSaleStatusInput input)
        {
            return await _mediator.Send(input);
        }

        [HttpPut("products/{id}")]
        public async Task<DefaultOutput<SaleViewModel>> Put(Guid id, [FromBody] UpdateSaleProductsInput input)
        {
            return await _mediator.Send(input);
        }

    }
}
