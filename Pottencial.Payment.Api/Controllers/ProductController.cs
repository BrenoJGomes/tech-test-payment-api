﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;

namespace Pottencial.Payment.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IMediator _mediator;

        public ProductController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<DefaultOutput<ProductViewModel>> Post([FromBody] AddProductInput request)
        {
            return await _mediator.Send(request);
        }

        [HttpGet("{id}")]
        public async Task<DefaultOutput<ProductViewModel>> Get(Guid id)
        {
            return await _mediator.Send(new FindProductInput() { Id = id});
        }

        [HttpGet]
        public async Task<DefaultOutput<IEnumerable<ProductViewModel>>> GetAll()
        {
            return await _mediator.Send(new GetAllProductsInput());
        }

        [HttpPut("{id}")]
        public async Task<DefaultOutput<ProductViewModel>> Put(Guid id, [FromBody] UpdateProductInput input)
        {
            return await _mediator.Send(input);
        }

        [HttpDelete("{id}")]
        public async Task<DefaultOutput> Delete(Guid id)
        {
            return await _mediator.Send(new DeleteProductInput() { Id = id});
        }
    }
}
