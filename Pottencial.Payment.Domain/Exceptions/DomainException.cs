﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.Domain.Exceptions
{
    /// <summary>
    /// Represents errors that occur within the domain.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DomainException(string message) : Exception(message)
    {
        /// <summary>
        /// Throws a DomainException if the specified rule is invalid.
        /// </summary>
        /// <param name="invalidRule">Indicates whether the rule is invalid.</param>
        /// <param name="message">The error message.</param>
        public static void ThrowWhen(bool invalidRule, string message)
        {
            if (invalidRule)
            {
                throw new DomainException(message);
            }
        }
    }
}
