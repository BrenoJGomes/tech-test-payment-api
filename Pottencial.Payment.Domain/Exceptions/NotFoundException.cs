﻿using System.Diagnostics.CodeAnalysis;

namespace Pottencial.Payment.Domain.Exceptions
{
    /// <summary>
    /// Exception thrown when a requested resource is not found.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class NotFoundException(string message) : Exception(message)
    {
        /// <summary>
        /// Throws a NotFoundException if the provided entity is null.
        /// </summary>
        /// <param name="entity">The entity to check.</param>
        /// <param name="errorMessage">The error message to include in the exception.</param>
        public static void ThrowWhenNullEntity(object? entity, string errorMessage)
        {
            if (entity is not null) return;
            throw new NotFoundException(errorMessage);
        }

        /// <summary>
        /// Throws a NotFoundException if the provided list is null or empty.
        /// </summary>
        /// <param name="list">The list to check.</param>
        /// <param name="errorMessage">The error message to include in the exception.</param>
        public static void ThrowWhenNullOrEmptyList(
            IEnumerable<object> list,
            string errorMessage)
        {
            if (list is not null && list.Any()) return;
            throw new NotFoundException(errorMessage);
        }
    }
}
