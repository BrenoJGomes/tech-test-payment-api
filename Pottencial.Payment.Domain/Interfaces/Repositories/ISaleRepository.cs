﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Domain.Interfaces.Repositories
{
    /// <summary>
    /// Represents a repository interface for managing sales.
    /// </summary>
    public interface ISaleRepository : IRepository<Sale, Guid>
    {
        Task DeleteSaleProducts(Sale entity);
    }
}
