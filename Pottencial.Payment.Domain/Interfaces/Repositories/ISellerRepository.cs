﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Domain.Interfaces.Repositories
{
    /// <summary>
    /// Represents a repository interface for managing sellers.
    /// </summary>
    public interface ISellerRepository : IRepository<Seller, Guid>
    {
    }
}
