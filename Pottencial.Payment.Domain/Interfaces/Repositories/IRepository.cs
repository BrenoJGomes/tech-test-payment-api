﻿using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces.Entities;
using System.Linq.Expressions;

namespace Pottencial.Payment.Domain.Interfaces.Repositories
{
    /// <summary>
    /// Represents a generic repository interface for managing entities.
    /// </summary>
    /// <typeparam name="T">The type of the entity.</typeparam>
    /// <typeparam name="TId">The type of the entity's identifier.</typeparam>
    public interface IRepository<T, in TId>
        where T : IEntity
        where TId : struct
    {
        /// <summary>
        /// Checks if an entity exists that matches the specified criteria.
        /// </summary>
        /// <param name="expression">The criteria to check for existence.</param>
        /// <returns>A task that represents the asynchronous operation, containing a boolean indicating existence.</returns>
        Task<bool> ExistsAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Finds entities that match the specified criteria.
        /// </summary>
        /// <param name="expression">The criteria to find entities.</param>
        /// <returns>A task that represents the asynchronous operation, containing a list of matching entities.</returns>
        Task<List<T>> FindAsync(Expression<Func<T, bool>> expression);

        /// <summary>
        /// Retrieves all entities.
        /// </summary>
        /// <returns>A task that represents the asynchronous operation, containing a list of all entities.</returns>
        Task<List<T>> GetAllAsync();

        /// <summary>
        /// Retrieves an entity by its identifier.
        /// </summary>
        /// <param name="id">The identifier of the entity.</param>
        /// <returns>A task that represents the asynchronous operation, containing the entity if found; otherwise, null.</returns>
        Task<T?> GetByIdAsync(TId id);

        /// <summary>
        /// Adds a new entity to the repository.
        /// </summary>
        /// <param name="entity">The entity to add.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task AddAsync(T entity);

        /// <summary>
        /// Updates an existing entity in the repository.
        /// </summary>
        /// <param name="entity">The entity to update.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task UpdateAsync(T entity);

        /// <summary>
        /// Deletes an entity from the repository.
        /// </summary>
        /// <param name="entity">The entity to delete.</param>
        /// <returns>A task that represents the asynchronous operation.</returns>
        Task Delete(T entity);

        /// <summary>
        /// Saves changes made to the repository.
        /// </summary>
        void SaveChanges();
    }
}
