﻿using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Domain.Interfaces.Repositories
{
    /// <summary>
    /// Interface for product repository operations.
    /// </summary>
    public interface IProductRepository : IRepository<Product, Guid>
    {
        Task<List<Product>> GetByIdListAsync(List<Guid> ids);
    }
}
