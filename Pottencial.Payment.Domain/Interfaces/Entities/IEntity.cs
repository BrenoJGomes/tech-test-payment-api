﻿namespace Pottencial.Payment.Domain.Interfaces.Entities
{
    /// <summary>
    /// Represents a base entity with an identifier and timestamps.
    /// </summary>
    public interface IEntity
    {
        /// <summary>
        /// Unique identifier of the entity.
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// Date and time when the entity was created.
        /// </summary>
        DateTime CreatedAt { get; }

        /// <summary>
        /// Date and time when the entity was last modified, if applicable.
        /// </summary>
        DateTime? ModifiedAt { get; }
    }
}
