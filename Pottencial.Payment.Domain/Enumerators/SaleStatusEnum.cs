﻿namespace Pottencial.Payment.Domain.Enumerators
{
    public enum SaleStatusEnum
    {
        AwaitingPayment,
        PaymentApproved,
        SentToCarrier,    
        Delivered,             
        Cancelled 
    }
}
