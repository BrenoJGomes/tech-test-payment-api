﻿namespace Pottencial.Payment.Domain.Enumerators
{
    public enum ProductCategoryEnum
    {
        Electronics,
        Clothing,
        HomeGoods,
        Books,
        Sports,
        Toys,
        Grocery,
        Health
    }
}
