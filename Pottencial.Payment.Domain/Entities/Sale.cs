﻿using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;

namespace Pottencial.Payment.Domain.Entities
{
    /// <summary>
    /// Represents a sale transaction.
    /// </summary>
    public class Sale : EntityBase
    {
        /// <summary>
        /// The identifier of the seller.
        /// </summary>
        public Guid SellerId { get; private set; }

        /// <summary>
        /// The seller associated with the sale.
        /// </summary>
        public Seller Seller { get; private set; }

        /// <summary>
        /// The date and time when the sale occurred.
        /// </summary>
        public DateTime SaleDate { get; private set; }

        /// <summary>
        /// The total amount of the sale.
        /// </summary>
        public decimal TotalAmount { get; private set; }

        /// <summary>
        /// The current status of the sale.
        /// </summary>
        public SaleStatusEnum Status { get; private set; }

        /// <summary>
        /// The list of products involved in the sale.
        /// </summary>
        public List<SaleProduct> Products { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sale"/> class.
        /// </summary>
        private Sale()
        {
            Products = new List<SaleProduct>();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Sale"/> class.
        /// </summary>
        /// <param name="seller">The seller associated with the sale.</param>
        /// <param name="products">The list of products involved in the sale.</param>
        /// <param name="status">The status of the sale.</param>
        public Sale(Seller seller)
        {
            SellerId = seller.Id;
            Seller = seller;
            Products = new List<SaleProduct>();
            Status = SaleStatusEnum.AwaitingPayment;
            SaleDate = DateTime.Now;
            TotalAmount = CalculateTotalAmount();
        }

        /// <summary>
        /// Calculates the total amount of the sale based on the products.
        /// </summary>
        /// <returns>The total amount of the sale.</returns>
        private decimal CalculateTotalAmount()
        {
            return Products.Sum(item => item.TotalPrice);
        }

        /// <summary>
        /// Updates the products in the sale if the status is "Awaiting Payment".
        /// </summary>
        /// <param name="products">The list of products to update.</param>
        public void UpdateProducts(List<SaleProduct> products)
        {
            if (products == null || !products.Any())
                throw new ArgumentException("At least one product must be provided.");

            Products = products;
            TotalAmount = CalculateTotalAmount();
        }

        /// <summary>
        /// Updates the status of the sale based on allowed transitions.
        /// </summary>
        /// <param name="newStatus">The new status to update to.</param>
        public void UpdateStatus(SaleStatusEnum newStatus)
        {
            DomainException.ThrowWhen(!IsValidStatusTransition(newStatus),$"Cannot transition from {Status} to {newStatus}.");
            Status = newStatus;
        }

        private bool IsValidStatusTransition(SaleStatusEnum newStatus)
        {
            switch (Status)
            {
                case SaleStatusEnum.AwaitingPayment:
                    return newStatus == SaleStatusEnum.PaymentApproved || newStatus == SaleStatusEnum.Cancelled;
                case SaleStatusEnum.PaymentApproved:
                    return newStatus == SaleStatusEnum.SentToCarrier || newStatus == SaleStatusEnum.Cancelled;
                case SaleStatusEnum.SentToCarrier:
                    return newStatus == SaleStatusEnum.Delivered;
                default:
                    return false;
            }
        }
    }
}
