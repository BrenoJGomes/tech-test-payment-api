﻿using System.Text.RegularExpressions;

namespace Pottencial.Payment.Domain.Entities
{
    /// <summary>
    /// Represents a seller entity with personal information.
    /// </summary>
    public class Seller : EntityBase
    {
        /// <summary>
        /// The name of the seller.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The CPF of the seller.
        /// </summary>
        public string Cpf { get; private set; }

        /// <summary>
        /// The email of the seller.
        /// </summary>
        public string Email { get; private set; }

        /// <summary>
        /// The phone number of the seller.
        /// </summary>
        public string Phone { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Seller"/> class.
        /// </summary>
        /// <param name="name">The name of the seller.</param>
        /// <param name="cpf">The CPF of the seller.</param>
        /// <param name="email">The email of the seller.</param>
        /// <param name="phone">The phone number of the seller.</param>
        public Seller(string name, string cpf, string email, string phone)
        {
            Name = name;
            Cpf = cpf;
            Email = email;
            Phone = phone;
        }

        public void Update(string name, string cpf, string email, string phone)
        {
            Name = name;
            Cpf = cpf;
            Email = email;
            Phone = phone;
            ModifiedAt = DateTime.Now;
        }
    }
}
