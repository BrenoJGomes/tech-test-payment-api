﻿using Pottencial.Payment.Domain.Enumerators;

namespace Pottencial.Payment.Domain.Entities
{
    /// <summary>
    /// Represents a product in the payment domain.
    /// </summary>
    public class Product : EntityBase
    {
        /// <summary>
        /// The name of the product.
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// The description of the product.
        /// </summary>
        public string Description { get; private set; }

        /// <summary>
        /// The price of the product.
        /// </summary>
        public decimal Price { get; private set; }

        /// <summary>
        /// The quantity of the product in stock.
        /// </summary>
        public int StockQuantity { get; private set; }

        /// <summary>
        /// The category of the product.
        /// </summary>
        public ProductCategoryEnum Category { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Product"/> class.
        /// </summary>
        /// <param name="name">The name of the product.</param>
        /// <param name="description">The description of the product.</param>
        /// <param name="price">The price of the product.</param>
        /// <param name="stockQuantity">The quantity of the product in stock.</param>
        /// <param name="category">The category of the product.</param>
        public Product(string name, string description, decimal price, int stockQuantity, ProductCategoryEnum category)
        {
            Name = name;
            Description = description;
            Price = price;
            StockQuantity = stockQuantity;
            Category = category;
        }

        /// <summary>
        /// Updates the product details.
        /// </summary>
        /// <param name="name">The new name of the product.</param>
        /// <param name="description">The new description of the product.</param>
        /// <param name="price">The new price of the product.</param>
        /// <param name="stockQuantity">The new quantity of the product in stock.</param>
        /// <param name="category">The new category of the product.</param>
        public void Update(string name, string description, decimal price, int stockQuantity, ProductCategoryEnum category)
        {
            Name = name;
            Description = description;
            Price = price;
            StockQuantity = stockQuantity;
            Category = category;
            ModifiedAt = DateTime.UtcNow;
        }
    }
}
