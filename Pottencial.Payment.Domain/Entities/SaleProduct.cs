﻿namespace Pottencial.Payment.Domain.Entities
{
    /// <summary>
    /// Represents a product sold in a sale.
    /// </summary>
    public class SaleProduct
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Sale Identifier
        /// </summary>
        public Guid SaleId { get; private set; }

        /// <summary>
        /// Sale Property
        /// </summary>
        public Sale Sale { get; private set; }

        /// <summary>
        /// Product Identifier
        /// </summary>
        public Guid ProductId { get; private set; }

        /// <summary>
        /// Product Property
        /// </summary>
        public Product Product { get; private set; }

        /// <summary>
        /// Quantity of the product sold.
        /// </summary>
        public int Quantity { get; private set; }

        /// <summary>
        /// Unit price of the product at the time of sale.
        /// </summary>
        public decimal UnitPrice { get; private set; }

        /// <summary>
        /// Calculates the total price for the item based on the quantity.
        /// </summary>
        public decimal TotalPrice => UnitPrice * Quantity;

        /// <summary>
        /// nitializes a new instance of the <see cref="SaleProduct"/> class.
        /// </summary>
        public SaleProduct()
        {    
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SaleProduct"/> class.
        /// </summary>
        public SaleProduct(Sale sale, Product product, int quantity, decimal unitPrice)
        {
            if (quantity <= 0)
                throw new ArgumentException("Quantity must be greater than zero.");

            Id = Guid.NewGuid();
            SaleId = sale.Id;
            ProductId = product.Id;
            Sale = sale;
            Product = product;
            Quantity = quantity;
            UnitPrice = unitPrice;
        }
    }
}