﻿using Pottencial.Payment.Domain.Interfaces.Entities;

namespace Pottencial.Payment.Domain.Entities
{
    /// <summary>
    /// Base class for entities, providing common properties and behavior.
    /// </summary>
    public class EntityBase : IEntity
    {
        /// <summary>
        /// Unique identifier for the entity.
        /// </summary>
        public Guid Id { get; }

        /// <summary>
        /// Date and time when the entity was created.
        /// </summary>
        public DateTime CreatedAt { get; protected set; }

        /// <summary>
        /// Date and time when the entity was last modified.
        /// </summary>
        public DateTime? ModifiedAt { get; protected set; }

        protected EntityBase()
        {
            Id = Guid.NewGuid();
            CreatedAt = DateTime.UtcNow;
            ModifiedAt = DateTime.UtcNow;
        }
    }
}
