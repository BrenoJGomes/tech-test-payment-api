﻿namespace Pottencial.Payment.Application.ViewModels.Sales
{
    /// <summary>
    /// Represents a sale transaction.
    /// </summary>
    public class SaleViewModel
    {
        /// <summary>
        /// Unique identifier for the sale.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Unique identifier for the seller.
        /// </summary>
        public Guid SellerId { get; set; }

        /// <summary>
        /// Name of the seller.
        /// </summary>
        public string SellerName { get; set; }

        /// <summary>
        /// Date when the sale occurred.
        /// </summary>
        public DateTime SaleDate { get; set; }

        /// <summary>
        /// Total amount of the sale.
        /// </summary>
        public decimal TotalAmount { get; set; }

        /// <summary>
        /// Current status of the sale.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// List of products involved in the sale.
        /// </summary>
        public List<SaleProductViewModel> Products { get; set; }

        public SaleViewModel()
        {
            Products = new List<SaleProductViewModel>();
        }
    }
}
