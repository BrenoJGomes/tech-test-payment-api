﻿namespace Pottencial.Payment.Application.ViewModels.Sales
{
    public class SaleProductViewModel
    {
        public Guid ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
