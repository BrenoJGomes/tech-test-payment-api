﻿using Pottencial.Payment.Domain.Enumerators;
using System.Diagnostics.CodeAnalysis;

namespace Pottencial.Payment.Application.ViewModels.Products
{
    [ExcludeFromCodeCoverage]
    public record ProductViewModel
    {
        /// <summary>
        /// Unique identifier for the product.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Description of the product.
        /// </summary>
        public string Description { get; init; }

        /// <summary>
        /// Price of the product.
        /// </summary>
        public decimal Price { get; init; }

        /// <summary>
        /// Quantity of the product in stock.
        /// </summary>
        public int StockQuantity { get; init; }

        /// <summary>
        /// Category of the product.
        /// </summary>
        public ProductCategoryEnum Category { get; init; }
    }
}
