﻿namespace Pottencial.Payment.Application.ViewModels.Sellers
{
    /// <summary>
    /// Represents a seller in the payment application.
    /// </summary>
    public record SellerViewModel
    {
        /// <summary>
        /// The unique identifier for the seller.
        /// </summary>
        public Guid Id { get; init; }

        /// <summary>
        /// The name of the seller.
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// The CPF (Cadastro de Pessoas Físicas) of the seller.
        /// </summary>
        public string Cpf { get; init; }

        /// <summary>
        /// The email address of the seller.
        /// </summary>
        public string Email { get; init; }

        /// <summary>
        /// The phone number of the seller.
        /// </summary>
        public string Phone { get; init; }
    }
}
