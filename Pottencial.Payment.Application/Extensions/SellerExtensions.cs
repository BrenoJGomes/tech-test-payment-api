﻿using Pottencial.Payment.Application.ViewModels.Sellers;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Application.Extensions
{
    public static class SellerExtensions
    {
        /// <summary>
        /// Converts a Seller entity to a SellerViewModel.
        /// </summary>
        /// <param name="seller">The seller entity to convert.</param>
        /// <returns>A SellerViewModel representing the seller.</returns>
        public static SellerViewModel NewSellerViewModel(this Seller seller)
        {
            return new SellerViewModel()
            {
                Id = seller.Id,
                Name = seller.Name,
                Cpf = seller.Cpf,
                Phone = seller.Phone
            };
        }

        /// <summary>
        /// Converts a list of Seller entities to a list of SellerViewModel.
        /// </summary>
        /// <param name="sellers">The list of seller entities to convert.</param>
        /// <returns>An IEnumerable of SellerViewModel representing the seller.</returns>
        public static IEnumerable<SellerViewModel> AsFindSellersViewModel(this List<Seller> sellers)
        {
            return sellers.Select(seller => new SellerViewModel()
            {
                Id = seller.Id,
                Name = seller.Name,
                Cpf = seller.Cpf,
                Phone = seller.Phone
            });
        }
    }
}
