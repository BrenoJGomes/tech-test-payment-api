﻿using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Application.Extensions
{
    public static class ProductExtensions
    {
        /// <summary>
        /// Converts a Product entity to a ProductViewModel.
        /// </summary>
        /// <param name="product">The product entity to convert.</param>
        /// <returns>A ProductViewModel representing the product.</returns>
        public static ProductViewModel NewProductViewModel(this Product product)
        {
            return new ProductViewModel()
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity,
                Category = product.Category
            };
        }

        /// <summary>
        /// Converts a list of Product entities to a list of ProductViewModels.
        /// </summary>
        /// <param name="products">The list of product entities to convert.</param>
        /// <returns>An IEnumerable of ProductViewModel representing the products.</returns>
        public static IEnumerable<ProductViewModel> AsFindProductsViewModel(this List<Product> products)
        {
            return products.Select(product => new ProductViewModel()
            {
                Id = product.Id,
                Name = product.Name,
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity,
                Category = product.Category
            });
        }
    }
}
