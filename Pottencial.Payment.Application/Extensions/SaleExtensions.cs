﻿using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Application.Extensions
{
    public static class SaleExtensions
    {
        /// <summary>
        /// Converts a Sale entity to a SaleViewModel.
        /// </summary>
        /// <param name="sale">The sale entity to convert.</param>
        /// <returns>A ProductViewModel representing the sale.</returns>
        public static SaleViewModel NewSaleViewModel(this Sale sale)
        {
            return new SaleViewModel()
            {
                Id = sale.Id,
                SellerId = sale.SellerId,
                SellerName = sale.Seller.Name,
                SaleDate = sale.SaleDate,
                TotalAmount = sale.TotalAmount,
                Status = sale.Status.ToString(),
                Products = sale.Products.AsSaleProductViewModel()
            };
        }

        /// <summary>
        /// Converts a list of Sale entities to a list of SaleViewModels.
        /// </summary>
        /// <param name="sales">The list of sale entities to convert.</param>
        /// <returns>An IEnumerable of SaleViewModel representing the sales.</returns>
        public static IEnumerable<SaleViewModel> AsFindSalesViewModel(this List<Sale> sales)
        {
            return sales.Select(sale => new SaleViewModel()
            {
                Id = sale.Id,
                SellerId = sale.SellerId,
                SellerName = sale.Seller.Name,
                SaleDate = sale.SaleDate,
                TotalAmount = sale.TotalAmount,
                Status = sale.Status.ToString(),
                Products = sale.Products.AsSaleProductViewModel()
            });
        }

        /// <summary>
        /// Converts a list of SaleProduct entities to a list of SaleProductViewModel.
        /// </summary>
        /// <param name="products">The list of SaleProduct entities to convert.</param>
        /// <returns>An List of SaleViewModel representing the SaleProduct.</returns>
        public static List<SaleProductViewModel> AsSaleProductViewModel(this List<SaleProduct> products)
        {
            return products.Select(product => new SaleProductViewModel()
            {
                ProductId = product.ProductId,
                ProductName = product.Product.Name,
                TotalPrice = product.TotalPrice,
            }).ToList();
        }
    }
}
