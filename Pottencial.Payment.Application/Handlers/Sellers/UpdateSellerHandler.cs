﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sellers;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sellers
{
    /// <summary>
    /// Handler for updating a seller.
    /// </summary>
    public class UpdateSellerHandler(ISellerRepository sellerRepository) : IRequestHandler<UpdateSellerInput, DefaultOutput<SellerViewModel>>
    {
        /// <summary>
        /// Handles the update seller request.
        /// </summary>
        /// <param name="request">The update seller input request.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the result of the update operation.</returns>
        public async Task<DefaultOutput<SellerViewModel>> Handle(UpdateSellerInput request, CancellationToken cancellationToken)
        {
            var seller = await sellerRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(seller, "No seller were found.");

            seller.Update(request.Name, request.Cpf, request.Email, request.Phone);
            await sellerRepository.UpdateAsync(seller);
            return new DefaultOutput<SellerViewModel>(true, "The seller was update successfully.", seller.NewSellerViewModel());
        }
    }
}
