﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sellers
{
    /// <summary>
    /// Handler for deleting a seller.
    /// </summary>
    public class DeleteSellerHandler(ISellerRepository sellerRepository) : IRequestHandler<DeleteSellerInput, DefaultOutput>
    {
        public async Task<DefaultOutput> Handle(DeleteSellerInput request, CancellationToken cancellationToken)
        {
            var seller = await sellerRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(seller, "The seller does not exist.");

            await sellerRepository.Delete(seller);
            return new DefaultOutput(true, "The seller was deleted successfully.");
        }
    }
}