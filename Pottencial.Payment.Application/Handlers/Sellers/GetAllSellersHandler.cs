﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sellers;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sellers
{
    /// <summary>
    /// Handler for retrieving all sellers.
    /// </summary>
    public class GetAllSellersHandler(ISellerRepository sellerRepository) : IRequestHandler<GetAllSellersInput, DefaultOutput<IEnumerable<SellerViewModel>>>
    {
        /// <summary>
        /// Handles the request to get all sellers.
        /// </summary>
        /// <param name="request">The request containing input parameters.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the output with the list of sellers.</returns>
        public async Task<DefaultOutput<IEnumerable<SellerViewModel>>> Handle(GetAllSellersInput request, CancellationToken cancellationToken)
        {
            var sellers = await sellerRepository.GetAllAsync();
            NotFoundException.ThrowWhenNullOrEmptyList(sellers, "No seller were found.");
            return new DefaultOutput<IEnumerable<SellerViewModel>>(true, "The sellers were found successfully.", sellers.AsFindSellersViewModel());
        }
    }
}
