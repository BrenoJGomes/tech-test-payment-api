﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sellers;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sellers
{
    /// <summary>
    /// Handler for finding a seller by its ID.
    /// Implements IRequestHandler to handle FindSellerInput requests.
    /// </summary>
    public class FindSellerHandler(ISellerRepository sellerRepository) : IRequestHandler<FindSellerInput, DefaultOutput<SellerViewModel>>
    {
        /// <summary>
        /// Handles the request to find a seller.
        /// </summary>
        /// <param name="request">The input request containing the seller ID.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A DefaultOutput containing the found SellerViewModel.</returns>
        public async Task<DefaultOutput<SellerViewModel>> Handle(FindSellerInput request, CancellationToken cancellationToken)
        {
            var seller = await sellerRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(seller, "The seller was not found.");
            return new DefaultOutput<SellerViewModel>(true, "The seller was found successfully.", seller.NewSellerViewModel());
        }
    }
}
