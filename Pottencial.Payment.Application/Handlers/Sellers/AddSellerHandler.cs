﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sellers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sellers
{
    /// <summary>
    /// Handler for adding a new seller.
    /// </summary>
    public class AddSellerHandler(ISellerRepository sellerRepository) : IRequestHandler<AddSellerInput, DefaultOutput<SellerViewModel>>
    {
        /// <summary>
        /// Handles the addition of a new seller.
        /// </summary>
        /// <param name="request">The input request containing seller details.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the result of the operation.</returns>
        public async Task<DefaultOutput<SellerViewModel>> Handle(AddSellerInput request, CancellationToken cancellationToken)
        {
            bool isSellerRegistered = await sellerRepository.ExistsAsync(p => p.Cpf == request.Cpf);
            DomainException.ThrowWhen(isSellerRegistered, "The seller is already registered.");

            Seller seller = new Seller(request.Name, request.Cpf, request.Email, request.Phone);
            await sellerRepository.AddAsync(seller);
            return new DefaultOutput<SellerViewModel>(true, "The seller was registered successfully.", seller.NewSellerViewModel());
        }
    }
}
