﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Products
{
    /// <summary>
    /// Handler for finding a product by its ID.
    /// Implements IRequestHandler to handle FindProductInput requests.
    /// </summary>
    public class FindProductHandler(IProductRepository productRepository) : IRequestHandler<FindProductInput, DefaultOutput<ProductViewModel>>
    {
        /// <summary>
        /// Handles the request to find a product.
        /// </summary>
        /// <param name="request">The input request containing the product ID.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A DefaultOutput containing the found ProductViewModel.</returns>
        public async Task<DefaultOutput<ProductViewModel>> Handle(FindProductInput request, CancellationToken cancellationToken)
        {
            var product = await productRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(product, "The product was not found.");
            return new DefaultOutput<ProductViewModel>(true, "The product was found successfully.", product.NewProductViewModel());
        }
    }
}
