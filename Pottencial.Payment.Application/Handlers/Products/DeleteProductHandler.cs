﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Products
{
    /// <summary>
    /// Handler for deleting a product.
    /// </summary>
    public class DeleteProductHandler(IProductRepository productRepository) : IRequestHandler<DeleteProductInput, DefaultOutput>
    {
        public async Task<DefaultOutput> Handle(DeleteProductInput request, CancellationToken cancellationToken)
        {
            var product = await productRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(product, "The product does not exist.");

            await productRepository.Delete(product);
            return new DefaultOutput(true, "The product was deleted successfully.");
        }
    }
}