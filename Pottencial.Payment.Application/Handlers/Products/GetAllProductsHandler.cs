﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Products
{
    /// <summary>
    /// Handler for retrieving all products.
    /// </summary>
    public class GetAllProductsHandler(IProductRepository productRepository) : IRequestHandler<GetAllProductsInput, DefaultOutput<IEnumerable<ProductViewModel>>>
    {
        /// <summary>
        /// Handles the request to get all products.
        /// </summary>
        /// <param name="request">The request containing input parameters.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the output with the list of products.</returns>
        public async Task<DefaultOutput<IEnumerable<ProductViewModel>>> Handle(GetAllProductsInput request, CancellationToken cancellationToken)
        {
            var products = await productRepository.GetAllAsync();
            NotFoundException.ThrowWhenNullOrEmptyList(products, "No products were found.");
            return new DefaultOutput<IEnumerable<ProductViewModel>>(true, "The products were found successfully.", products.AsFindProductsViewModel());
        }
    }
}
