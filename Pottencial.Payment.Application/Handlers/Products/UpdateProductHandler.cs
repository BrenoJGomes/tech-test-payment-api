﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Products
{
    /// <summary>
    /// Handler for updating a product.
    /// </summary>
    public class UpdateProductHandler(IProductRepository productRepository) : IRequestHandler<UpdateProductInput, DefaultOutput<ProductViewModel>>
    {
        /// <summary>
        /// Handles the update product request.
        /// </summary>
        /// <param name="request">The update product input request.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the result of the update operation.</returns>
        public async Task<DefaultOutput<ProductViewModel>> Handle(UpdateProductInput request, CancellationToken cancellationToken)
        {
            var product = await productRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(product, "No products were found.");

            product.Update(request.Name, request.Description, request.Price, request.StockQuantity, request.Category);
            await productRepository.UpdateAsync(product);
            return new DefaultOutput<ProductViewModel>(true, "The product was update successfully.", product.NewProductViewModel());
        }
    }
}
