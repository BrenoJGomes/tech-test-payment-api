﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Products
{
    /// <summary>
    /// Handler for adding a new product.
    /// </summary>
    public class AddProductHandler(IProductRepository productRepository) : IRequestHandler<AddProductInput, DefaultOutput<ProductViewModel>>
    {
        /// <summary>
        /// Handles the addition of a new product.
        /// </summary>
        /// <param name="request">The input request containing product details.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the result of the operation.</returns>
        public async Task<DefaultOutput<ProductViewModel>> Handle(AddProductInput request, CancellationToken cancellationToken)
        {
            bool isProductRegistered = await productRepository.ExistsAsync(p => p.Name == request.Name);
            DomainException.ThrowWhen(isProductRegistered, "The product is already registered.");

            Product product = new Product(request.Name, request.Description, request.Price, request.StockQuantity, request.Category);
            await productRepository.AddAsync(product);
            return new DefaultOutput<ProductViewModel>(true, "The product was registered successfully.", product.NewProductViewModel());
        }
    }
}
