﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sales
{
    /// <summary>
    /// Handler for update sale.
    /// </summary>
    public class UpdateSaleProductsHandler(ISaleRepository saleRepository, IProductRepository productRepository) : IRequestHandler<UpdateSaleProductsInput, DefaultOutput<SaleViewModel>>
    {
        /// <summary>
        /// Handles the update products a sale.
        /// </summary>
        /// <param name="request">The input request containing salle details.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the result of the operation.</returns>
        public async Task<DefaultOutput<SaleViewModel>> Handle(UpdateSaleProductsInput request, CancellationToken cancellationToken)
        {
            var sale = await saleRepository.GetByIdAsync(request.SaleId);
            NotFoundException.ThrowWhenNullEntity(sale, "No sales were found.");
            DomainException.ThrowWhen(sale.Status != SaleStatusEnum.AwaitingPayment, "Cannot update products unless the sale is awaiting payment.");

            
            var saleProducts = CreateSaleProducts(request.Products, sale);

            await saleRepository.DeleteSaleProducts(sale);
            sale.UpdateProducts(saleProducts);

            await saleRepository.AddAsync(sale);
            return new DefaultOutput<SaleViewModel>(true, "The sale was registered successfully.", sale.NewSaleViewModel());
        }

        /// <summary>
        /// Creates a list of SaleProduct objects based on the input data.
        /// </summary>
        /// <param name="saleProductInput"></param>
        /// <param name="sale"></param>
        /// <returns>Return a list of a SaleProduct</returns>
        private List<SaleProduct> CreateSaleProducts(List<SaleProductInput> saleProductInput, Sale sale)
        {
            var saleProductList = new List<SaleProduct>();
            var idList = saleProductInput.Select(x => x.ProductId).ToList();
            List<Product> products = productRepository.GetByIdListAsync(idList).Result;

            var missingIds = idList.Except(products.Select(p => p.Id)).ToList();
            DomainException.ThrowWhen(missingIds.Any(), $"The following product IDs were not found: {string.Join(", ", missingIds)}");

            foreach (var productInput in saleProductInput)
            {
                var productInDb = products.FirstOrDefault(p => p.Id == productInput.ProductId);
                var saleProduct = new SaleProduct(sale, productInDb, productInput.Quantity, productInDb.Price);
                saleProductList.Add(saleProduct);
            }

            return saleProductList;
        }
    }
}