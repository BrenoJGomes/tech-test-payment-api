﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sales
{
    /// <summary>
    /// Handler for retrieving all sales.
    /// </summary>
    public class GetAllSalesHandler(ISaleRepository saleRepository) : IRequestHandler<GetAllSalesInput, DefaultOutput<IEnumerable<SaleViewModel>>>
    {
        /// <summary>
        /// Handles the request to get all sales.
        /// </summary>
        /// <param name="request">The request containing input parameters.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the output with the list of sales.</returns>
        public async Task<DefaultOutput<IEnumerable<SaleViewModel>>> Handle(GetAllSalesInput request, CancellationToken cancellationToken)
        {
            var sales = await saleRepository.GetAllAsync();
            NotFoundException.ThrowWhenNullOrEmptyList(sales, "No sales were found.");
            return new DefaultOutput<IEnumerable<SaleViewModel>>(true, "The sales were found successfully.", sales.AsFindSalesViewModel());
        }
    }
}
