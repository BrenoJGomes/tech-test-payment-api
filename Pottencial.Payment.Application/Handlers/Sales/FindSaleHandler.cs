﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sales
{
    /// <summary>
    /// Handler for finding a sale by its ID.
    /// Implements IRequestHandler to handle FindSaleInput requests.
    /// </summary>
    public class FindSaleHandler(ISaleRepository saleRepository) : IRequestHandler<FindSaleInput, DefaultOutput<SaleViewModel>>
    {
        /// <summary>
        /// Handles the request to find a sale.
        /// </summary>
        /// <param name="request">The input request containing the sale ID.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A DefaultOutput containing the found SaleViewModel.</returns>
        public async Task<DefaultOutput<SaleViewModel>> Handle(FindSaleInput request, CancellationToken cancellationToken)
        {
            var sale = await saleRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(sale, "The sale was not found.");
            return new DefaultOutput<SaleViewModel>(true, "The sale was found successfully.", sale.NewSaleViewModel());
        }
    }
}
