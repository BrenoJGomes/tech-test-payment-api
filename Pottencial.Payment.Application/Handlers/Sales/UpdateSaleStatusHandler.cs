﻿using MediatR;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.Extensions;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.Application.Handlers.Sales
{
    /// <summary>
    /// Handler for updating a sale status.
    /// </summary>
    public class UpdateSaleStatusHandler(ISaleRepository saleRepository) : IRequestHandler<UpdateSaleStatusInput, DefaultOutput<SaleViewModel>>
    {
        /// <summary>
        /// Handles the update sale status request.
        /// </summary>
        /// <param name="request">The update sale status input request.</param>
        /// <param name="cancellationToken">Cancellation token for the operation.</param>
        /// <returns>A task that represents the asynchronous operation, containing the result of the update operation.</returns>
        public async Task<DefaultOutput<SaleViewModel>> Handle(UpdateSaleStatusInput request, CancellationToken cancellationToken)
        {
            var sale = await saleRepository.GetByIdAsync(request.Id);
            NotFoundException.ThrowWhenNullEntity(sale, "No sales were found.");
                
            sale.UpdateStatus(request.Status);
            saleRepository.SaveChanges();
            return new DefaultOutput<SaleViewModel>(true, "The product was update successfully.", sale.NewSaleViewModel());
        }
    }
}
