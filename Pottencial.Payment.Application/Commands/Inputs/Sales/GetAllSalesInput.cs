﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sales;

namespace Pottencial.Payment.Application.Commands.Inputs.Sales
{
    /// <summary>
    /// Represents the input for the GetAllSales command.
    /// </summary>
    public class GetAllSalesInput : IRequest<DefaultOutput<IEnumerable<SaleViewModel>>>
    {
    }
}
