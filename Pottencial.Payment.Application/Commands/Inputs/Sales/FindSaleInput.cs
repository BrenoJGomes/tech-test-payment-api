﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sales;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Sales
{
    /// <summary>
    /// Represents the input for finding a sale.
    /// </summary>
    public class FindSaleInput : IRequest<DefaultOutput<SaleViewModel>>
    {
        /// <summary>
        /// Sale identifier.
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
