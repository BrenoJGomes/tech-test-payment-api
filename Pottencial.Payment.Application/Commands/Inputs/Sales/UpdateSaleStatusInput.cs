﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Application.ViewModels.Sales;
using Pottencial.Payment.Domain.Enumerators;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Sales
{
    /// <summary>
    /// Represents the input for updating a sale.
    /// </summary>
    public class UpdateSaleStatusInput : IRequest<DefaultOutput<SaleViewModel>>
    {
        /// <summary>
        /// Unique identifier of the sales.
        /// </summary>
        [Required(ErrorMessage = "Id is required.")]
        public Guid Id { get; set; }

        /// <summary>
        /// Sale status.
        /// </summary>
        [Required(ErrorMessage = "Status is required.")]
        public SaleStatusEnum Status { get; set; }
    }
}
