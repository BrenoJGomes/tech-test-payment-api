﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sales;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Sales
{
    /// <summary>
    /// Represents the input data required to add a new sale.
    /// </summary>
    public class UpdateSaleProductsInput : IRequest<DefaultOutput<SaleViewModel>>
    {
        /// <summary>
        /// The identifier of the sale.
        /// </summary>
        [Required(ErrorMessage = "Sale ID is required.")]
        public Guid SaleId { get; set; }

        /// <summary>
        /// The list of products involved in the sale.
        /// </summary>
        [Required(ErrorMessage = "At least one product must be provided.")]
        public List<SaleProductInput> Products { get; set; }
    }
}
