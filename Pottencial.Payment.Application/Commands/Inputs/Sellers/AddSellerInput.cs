﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sellers;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Sellers
{
    /// <summary>
    /// Represents the input data required to add a new seller.
    /// </summary>
    public class AddSellerInput : IRequest<DefaultOutput<SellerViewModel>>
    {
        /// <summary>
        /// The name of the seller.
        /// </summary>
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(100, ErrorMessage = "Name cannot be longer than 100 characters.")]
        public string Name { get; set; }

        /// <summary>
        /// The CPF of the seller.
        /// </summary>
        [Required(ErrorMessage = "CPF is required.")]
        [RegularExpression(@"^\d{11}$", ErrorMessage = "CPF must contain exactly 11 digits.")]
        public string Cpf { get; set; }

        /// <summary>
        /// The email of the seller.
        /// </summary>
        [Required(ErrorMessage = "Email is required.")]
        [EmailAddress(ErrorMessage = "Invalid email format.")]
        public string Email { get; set; }

        /// <summary>
        /// The phone number of the seller.
        /// </summary>
        [Required(ErrorMessage = "Phone is required.")]
        public string Phone { get; set; }
    }
}
