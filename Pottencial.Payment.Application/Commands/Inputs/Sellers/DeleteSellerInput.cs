﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Sellers
{
    /// <summary>
    /// Represents the input for deleting a seller.
    /// </summary>
    public class DeleteSellerInput : IRequest<DefaultOutput>
    {
        /// <summary>
        /// Seller identifier.
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
