﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Application.ViewModels.Sellers;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Sellers
{
    /// <summary>
    /// Represents the input for finding a seller.
    /// </summary>
    public class FindSellerInput : IRequest<DefaultOutput<SellerViewModel>>
    {
        /// <summary>
        /// Seller identifier.
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
