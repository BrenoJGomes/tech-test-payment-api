﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Sellers;

namespace Pottencial.Payment.Application.Commands.Inputs.Sellers
{
    /// <summary>
    /// Represents the input for the GetAllSellers command.
    /// </summary>
    public class GetAllSellersInput : IRequest<DefaultOutput<IEnumerable<SellerViewModel>>>
    {
    }
}
