﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Products
{
    /// <summary>
    /// Represents the input for deleting a product.
    /// </summary>
    public class DeleteProductInput : IRequest<DefaultOutput>
    {
        /// <summary>
        /// Product identifier.
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
