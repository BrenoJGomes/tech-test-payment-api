﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Enumerators;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Products
{
    /// <summary>
    /// Represents the input data required to add a new product.
    /// </summary>
    public class AddProductInput : IRequest<DefaultOutput<ProductViewModel>>
    {
        /// <summary>
        /// The name of the product.
        /// </summary>
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        public string Name { get; set; }

        /// <summary>
        /// The description of the product.
        /// </summary>
        [Required(ErrorMessage = "Description is required.")]
        [StringLength(500, ErrorMessage = "Description cannot be longer than 500 characters.")]
        public string Description { get; set; }

        /// <summary>
        /// The price of the product.
        /// </summary>
        [Required(ErrorMessage = "Price is required.")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Price must be greater than zero.")]
        public decimal Price { get; set; }

        /// <summary>
        /// The stock quantity of the product.
        /// </summary>
        [Required(ErrorMessage = "Stock quantity is required.")]
        [Range(0, int.MaxValue, ErrorMessage = "Stock quantity cannot be negative.")]
        public int StockQuantity { get; set; }

        /// <summary>
        /// The category of the product.
        /// </summary>
        [Required(ErrorMessage = "Category is required.")]
        public ProductCategoryEnum Category { get; set; }
    }
}
