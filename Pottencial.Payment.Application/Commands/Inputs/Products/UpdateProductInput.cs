﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;
using Pottencial.Payment.Domain.Enumerators;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Products
{
    /// <summary>
    /// Represents the input for updating a product.
    /// </summary>
    public class UpdateProductInput : IRequest<DefaultOutput<ProductViewModel>>
    {
        /// <summary>
        /// Unique identifier of the product.
        /// </summary>
        [Required(ErrorMessage = "Id is required.")]
        public Guid Id { get; set; }

        /// <summary>
        /// Name of the product.
        /// </summary>
        [Required(ErrorMessage = "Name is required.")]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        public string Name { get; set; }

        /// <summary>
        /// Description of the product.
        /// </summary>
        [Required(ErrorMessage = "Description is required.")]
        [StringLength(500, ErrorMessage = "Description cannot be longer than 500 characters.")]
        public string Description { get; set; }

        /// <summary>
        /// Price of the product.
        /// </summary>
        [Required(ErrorMessage = "Price is required.")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Price must be greater than zero.")]
        public decimal Price { get; set; }

        /// <summary>
        /// Stock quantity of the product.
        /// </summary>
        [Required(ErrorMessage = "Stock quantity is required.")]
        [Range(0, int.MaxValue, ErrorMessage = "Stock quantity cannot be negative.")]
        public int StockQuantity { get; set; }

        /// <summary>
        /// Category of the product.
        /// </summary>
        [Required(ErrorMessage = "Category is required.")]
        public ProductCategoryEnum Category { get; set; }
    }
}
