﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;

namespace Pottencial.Payment.Application.Commands.Inputs.Products
{
    /// <summary>
    /// Represents the input for the GetAllProducts command.
    /// </summary>
    public class GetAllProductsInput : IRequest<DefaultOutput<IEnumerable<ProductViewModel>>>
    {
    }
}
