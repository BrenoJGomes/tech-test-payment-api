﻿using MediatR;
using Pottencial.Payment.Application.Commands.Outputs;
using Pottencial.Payment.Application.ViewModels.Products;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Payment.Application.Commands.Inputs.Products
{
    /// <summary>
    /// Represents the input for finding a product.
    /// </summary>
    public class FindProductInput : IRequest<DefaultOutput<ProductViewModel>>
    {
        /// <summary>
        /// Product identifier.
        /// </summary>
        [Required]
        public Guid Id { get; set; }
    }
}
