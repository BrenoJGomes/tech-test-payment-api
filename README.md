## Entrega do teste técnico

Devido às demandas do trabalho e da pós-graduação, não foi possível realizar algumas das etapas desejadas em decorrência do tempo. No entanto, foi uma ótima experiência a realização do teste. 
No diretório "Evidencias" subi alguns prints da execução da API, quaLquer coisa,estou à disposição.

## Resumo da Aplicação

Esta aplicação é uma API REST desenvolvida para gerenciar vendas, seguindo as diretrizes estabelecidas no teste técnico da Pottencial. A API permite o registro de vendas, gerenciamento de produtos e atualização de status, garantindo que todas as operações respeitem as regras de negócio definidas.

### Funcionalidades Implementadas

1. **Registro de Vendas**: Permite o registro de uma venda com os dados do vendedor e os itens vendidos. Cada venda é associada a um vendedor que possui ID, CPF, nome, e-mail e telefone.

2. **Gerenciamento de produtos**: É possível incluir pelo menos um item na venda e, enquanto a venda estiver com status "Aguardando Pagamento", novos itens podem ser adicionados ou itens existentes podem ser removidos.

3. **Atualização de Status**: A API permite a atualização do status da venda, respeitando as transições permitidas:
   - De "Aguardando Pagamento" para "Pagamento Aprovado" ou "Cancelada".
   - De "Pagamento Aprovado" para "Enviado para Transportadora" ou "Cancelada".
   - De "Enviado para Transportadora" para "Entregue".

4. **Consulta de Vendas**: É possível obter informações de uma venda através do seu ID.

5. **Cadastro e Manutenção**: Foram desenvolvidos endpoints para o cadastro e manutenção dos produtos, vendedores e das vendas.

### Tecnologias Utilizadas

- **.NET Core**: Para o desenvolvimento da API.
- **Entity Framework**: Para persistência de dados no SqlServer.
- **XUnit**: Para a implementação de testes unitários.

### Estrutura do Projeto

A aplicação foi estruturada seguindo os princípios de Clean Architecture, separando as camadas de UI, orquestração e domínio, garantindo uma manutenção mais fácil e escalabilidade.
Foram implementados testes unitários para garantir a funcionalidade e a integridade do código, seguindo as boas práticas de desenvolvimento.

### Instruções para Execução da Aplicação
1. **Clone o repositório**
2. **Executar o docker-compose up para subir o container do sqlServer**
3. **Executar o comando dotnet ef database update --project Pottencial.Payment.Infra --startup-project Pottencial.Payment.Apipara criar o banco de dados**


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------



## INSTRUÇÕES PARA O TESTE TÉCNICO

- Crie um fork deste projeto (https://gitlab.com/Pottencial/tech-test-payment-api/-/forks/new). É preciso estar logado na sua conta Gitlab;
- Adicione @Pottencial (Pottencial Seguradora) como membro do seu fork. Você pode fazer isto em  https://gitlab.com/`your-user`/tech-test-payment-api/settings/members;
 - Quando você começar, faça um commit vazio com a mensagem "Iniciando o teste de tecnologia" e quando terminar, faça o commit com uma mensagem "Finalizado o teste de tecnologia";
 - Commit após cada ciclo de refatoração pelo menos;
 - Não use branches;
 - Você deve prover evidências suficientes de que sua solução está completa indicando, no mínimo, que ela funciona;

## O TESTE

A Pottencial necessita criar uma API para manter vendas, com as seguintes regras:

1) Deve ser possível o registro de uma venda, que consiste nos dados do vendedor + itens vendidos; 
2) Uma venda contém informação sobre o vendedor que a efetivou, data, identificador do pedido e os itens que foram vendidos;
3) O vendedor deve possuir id, cpf, nome, e-mail e telefone;
4) A inclusão de uma venda deve possuir pelo menos 1 item;
5) Após o registro da venda ela deverá ficar com status "Aguardando Pagamento";
6) Deve ser possível obter uma venda através do seu ID;
7) Deve ser possível incluir novos itens ou remover itens, enquanto a venda ainda estiver com status "Aguardando Pagamento”, observando o item 4;
8) Deve ser possível atualizar o status de uma venda informando seu ID e algum dos status: 
`Pagamento aprovado` | `Enviado para transportadora` | `Entregue` | `Cancelada`;
 
9) Deve ser respeitada a seguinte regra de atualização de status:
 
De: `Aguardando pagamento` Para: `Pagamento Aprovado`
De: `Aguardando pagamento` Para: `Cancelada`
De: `Pagamento Aprovado` Para: `Enviado para Transportadora`
De: `Pagamento Aprovado` Para: `Cancelada`
De: `Enviado para Transportador` Para: `Entregue`

## ORIENTAÇÕES TÉCNICAS
 
## Obrigatório:
 
- Utilização do padrão REST;
- Linguagens de preferência: .Net Core e NodeJs (com Typescript);
- Testes de unidade;
- Persistir dados mesmo que "em memória";
 
## Desejável:
- Mecanismos de autenticação/autorização;
- Utilizar Micro ORM ou ORM (Ex. Dapper, Entity) para manipular os dados;
- Testes de integração;
- Middleware para tratamento de erros da aplicação;
- Docker para subir as dependências (Ex. banco de dados, Mensageria);
- Aplicar arquitetura limpa ou outro design de aplicação que separe as camadas de UI, orquestração e entidade de domínio;

## PONTOS QUE SERÃO AVALIADOS
- Uso correto do padrão REST;
- Arquitetura da aplicação, iremos avaliar como o projeto foi estruturada, bem como camadas e suas responsabilidades;
- Programação orientada a objetos;
- Boas práticas e princípios como Clean Code, SOLID, DRY, KISS;
- Se for utilizado DDD, clean architecture ou outro padrão arquitetural será avaliada a aplicação correta dos conceitos; 
- Qualidade dos Testes;
- Profissionalismo do código.


