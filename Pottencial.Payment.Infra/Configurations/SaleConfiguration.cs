﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Infra.Configurations
{
    public class SaleConfiguration : IEntityTypeConfiguration<Sale>
    {
        /// <summary>
        /// Configures the entity properties and relationships for Sale.
        /// </summary>
        /// <param name="builder">The builder used to configure the entity.</param>
        public void Configure(EntityTypeBuilder<Sale> builder)
        {
            builder.HasKey(e => e.Id);
            builder.Property(e => e.SellerId).IsRequired();
            builder.Property(e => e.SaleDate).IsRequired();
            builder.Property(e => e.TotalAmount).HasColumnType("decimal(18,2)");
            builder.Property(e => e.Status).IsRequired();

            builder.HasOne(e => e.Seller)
                .WithMany()
                .HasForeignKey(e => e.SellerId);

            builder.HasMany(e => e.Products)
                .WithOne(sp => sp.Sale)
                .HasForeignKey(sp => sp.SaleId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.Navigation(e => e.Products).AutoInclude();
            builder.Navigation(e => e.Seller).AutoInclude();

        }
    }
}
