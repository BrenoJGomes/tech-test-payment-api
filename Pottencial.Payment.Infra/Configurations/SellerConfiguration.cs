﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.Infra.Configurations
{
    public class SellerConfiguration : IEntityTypeConfiguration<Seller>
    {
        /// <summary>
        /// Configures the entity properties and relationships for Seller.
        /// </summary>
        /// <param name="builder">The builder used to configure the entity.</param>
        public void Configure(EntityTypeBuilder<Seller> builder)
        {
               builder.HasKey(p => p.Id);
    
                builder.Property(p => p.Name)
                 .IsRequired()
                 .HasMaxLength(100);
    
                builder.Property(p => p.Cpf)
                 .IsRequired()
                 .HasMaxLength(11);
    
                builder.Property(p => p.Email)
                 .IsRequired()
                 .HasMaxLength(50);
    
                builder.Property(p => p.Phone)
                 .IsRequired()
                 .HasMaxLength(20);
        }
    }
}
