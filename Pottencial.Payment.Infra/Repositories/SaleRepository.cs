﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces.Repositories;
using Pottencial.Payment.Infra.Contexts;
using System.Linq.Expressions;

namespace Pottencial.Payment.Infra.Repositories
{
    /// <summary>
    /// Repository for managing sale entities.
    /// </summary>
    /// <param name="dbContext"></param>
    public class SaleRepository(AppDbContext dbContext) : ISaleRepository
    {
        private readonly AppDbContext _dbContext = dbContext;


        public async Task AddAsync(Sale entity)
        {
            await _dbContext.Sales.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task Delete(Sale entity)
        {
            _dbContext.Sales.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> ExistsAsync(Expression<Func<Sale, bool>> expression)
        {
            return await _dbContext.Sales.AnyAsync(expression);
        }

        public async Task<List<Sale>> FindAsync(Expression<Func<Sale, bool>> expression)
        {
            return await _dbContext.Sales.Where(expression).ToListAsync();
        }

        public async Task<List<Sale>> GetAllAsync()
        {
            return await _dbContext.Sales.ToListAsync();
        }

        public async Task<Sale?> GetByIdAsync(Guid id)
        {
            return await _dbContext.Sales.FindAsync(id);
        }

        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        

        public async Task UpdateAsync(Sale entity)
        {
            _dbContext.Sales.Update(entity);
            await _dbContext.SaveChangesAsync();
        }
        public async Task DeleteSaleProducts(Sale entity)
        {
            var saleProducts = await _dbContext.SaleProducts
                .Where(sp => sp.SaleId == entity.Id)
                .ToListAsync();

            _dbContext.SaleProducts.RemoveRange(saleProducts);
            _dbContext.Sales.Remove(entity);
            await _dbContext.SaveChangesAsync();
        }
    }
}
