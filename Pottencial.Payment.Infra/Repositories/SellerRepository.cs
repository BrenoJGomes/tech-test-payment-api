﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces.Repositories;
using Pottencial.Payment.Infra.Contexts;
using System.Linq.Expressions;

namespace Pottencial.Payment.Infra.Repositories
{
    /// <summary>
    /// Repository class for managing Seller entities in the database.
    /// Implements ISellerRepository interface.
    /// </summary>
    public class SellerRepository(AppDbContext dbContext) : ISellerRepository
    {
        private readonly AppDbContext _dbContext = dbContext;

        /// <summary>
        /// Add a new Seller entity to the database.
        /// </summary>
        /// <param name="entity">The Seller entity to add.</param>
        public async Task AddAsync(Seller entity)
        {
            await _dbContext.Sellers.AddAsync(entity);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a Seller entity from the database.
        /// </summary>
        /// <param name="entity">The Seller entity to delete.</param>
        public async Task Delete(Seller entity)
        {
            _dbContext.Sellers.Remove(entity);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Checks if a Seller entity exists based on the provided expression.
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>True if the entity exists; otherwise, false.</returns>
        public async Task<bool> ExistsAsync(Expression<Func<Seller, bool>> expression)
        {
            return await _dbContext.Sellers.AnyAsync(expression);
        }

        /// <summary>
        /// Finds Seller entities based on the provided expression.
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>A list of matching Seller entities.</returns>
        public async Task<List<Seller>> FindAsync(Expression<Func<Seller, bool>> expression)
        {
            return await _dbContext.Sellers.Where(expression).ToListAsync();
        }

        /// <summary>
        /// Retrieves all Seller entities from the database.
        /// </summary>
        /// <returns>A list of all Seller entities.</returns>
        public async Task<List<Seller>> GetAllAsync()
        {
            return await _dbContext.Sellers.ToListAsync();
        }

        /// <summary>
        /// Retrieves a Seller entity by its unique identifier.
        /// </summary>
        /// <param name="id">The unique identifier of the Seller.</param>
        /// <returns>The Seller entity if found; otherwise, null.</returns>
        public async Task<Seller?> GetByIdAsync(Guid id)
        {
            return await _dbContext.Sellers.FindAsync(id);
        }

        /// <summary>
        /// Save changes made to the database synchronously.
        /// </summary>
        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Save changes made to the database.
        /// </summary>
        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Update an existing Seller entity in the database.
        /// </summary>
        /// <param name="entity">The Seller entity to update.</param>
        public async Task UpdateAsync(Seller entity)
        {
            _dbContext.Sellers.Update(entity);
            await SaveChangesAsync();
        }
    }
}
