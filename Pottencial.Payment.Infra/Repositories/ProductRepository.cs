﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces.Repositories;
using Pottencial.Payment.Infra.Contexts;
using System.Linq.Expressions;

namespace Pottencial.Payment.Infra.Repositories
{
    /// <summary>
    /// Repository for managing product entities.
    /// </summary>
    public class ProductRepository(AppDbContext dbContext) : IProductRepository
    {
        private readonly AppDbContext _dbContext = dbContext;

        /// <summary>
        /// Adds a new product entity asynchronously.
        /// </summary>
        /// <param name="entity">The product entity to add.</param>
        public async Task AddAsync(Product entity)
        {
            await _dbContext.Products.AddAsync(entity);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Checks if a product exists based on the provided expression.
        /// </summary>
        /// <param name="expression">The expression to evaluate.</param>
        /// <returns>True if the product exists; otherwise, false.</returns>
        public async Task<bool> ExistsAsync(Expression<Func<Product, bool>> expression)
        {
            return await _dbContext.Products.AnyAsync(expression);
        }

        /// <summary>
        /// Finds products based on the provided expression.
        /// </summary>
        /// <param name="expression">The expression to filter products.</param>
        /// <returns>A list of products that match the expression.</returns>
        public async Task<List<Product>> FindAsync(Expression<Func<Product, bool>> expression)
        {
            return await _dbContext.Products.Where(expression).ToListAsync();
        }

        /// <summary>
        /// Retrieves all products asynchronously.
        /// </summary>
        /// <returns>A list of all products.</returns>
        public async Task<List<Product>> GetAllAsync()
        {
            return await _dbContext.Products.ToListAsync();
        }

        /// <summary>
        /// Retrieves a product by its identifier asynchronously.
        /// </summary>
        /// <param name="id">The identifier of the product.</param>
        /// <returns>The product entity if found; otherwise, null.</returns>
        public async Task<Product?> GetByIdAsync(Guid id)
        {
            return await _dbContext.Products.FindAsync(id);
        }

        /// <summary>
        /// Saves changes to the database.
        /// </summary>
        public void SaveChanges()
        {
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Saves changes to the database asynchronously.
        /// </summary>
        public async Task SaveChangesAsync()
        {
            await _dbContext.SaveChangesAsync();
        }

        /// <summary>
        /// Updates an existing product entity asynchronously.
        /// </summary>
        /// <param name="product">The product entity to update.</param>
        public async Task UpdateAsync(Product product)
        {
            _dbContext.Products.Update(product);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Deletes a product entity asynchronously.
        /// </summary>
        /// <param name="product">The product entity to delete.</param>
        public async Task Delete(Product product)
        {
            _dbContext.Products.Remove(product);
            await SaveChangesAsync();
        }

        /// <summary>
        /// Retrieves products by a list of identifiers asynchronously.
        /// </summary>
        /// <param name="ids">The list of product identifiers.</param>
        /// <returns>A list of products that match the identifiers.</returns>
        public async Task<List<Product>> GetByIdListAsync(List<Guid> ids)
        {
            return await _dbContext.Products.Where(product => ids.Contains(product.Id)).ToListAsync();
        }
    }
}
