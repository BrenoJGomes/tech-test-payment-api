﻿using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Handlers.Sales;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sales
{
    public class FindSaleHandlerTests
    {
        private readonly Mock<ISaleRepository> _saleRepositoryMock;
        private readonly FindSaleHandler _handler;

        public FindSaleHandlerTests()
        {
            _saleRepositoryMock = new Mock<ISaleRepository>();
            _handler = new FindSaleHandler(_saleRepositoryMock.Object);

        }

        [Fact]
        public async Task GivenSaleExists_WhenHandleCalled_ThenReturnsSaleViewModel()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var sale = new Sale(seller);

            _saleRepositoryMock.Setup(repo => repo.GetByIdAsync(sale.Id)).ReturnsAsync(sale);
            var request = new FindSaleInput { Id = sale.Id };

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The sale was found successfully.", result.Message);
            Assert.NotNull(result.Data);
        }
    }
}
