﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Handlers.Sales;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sales
{
    public class AddSalesHandlerTests
    {
        private readonly Mock<ISaleRepository> _saleRepositoryMock;
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly Mock<ISellerRepository> _sellerRepositoryMock;
        private readonly AddSaleHandler _handler;
        private readonly Faker _faker;

        public AddSalesHandlerTests()
        {
            _saleRepositoryMock = new Mock<ISaleRepository>();
            _productRepositoryMock = new Mock<IProductRepository>();
            _sellerRepositoryMock = new Mock<ISellerRepository>();
            _handler = new AddSaleHandler(_saleRepositoryMock.Object, _productRepositoryMock.Object, _sellerRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSellerNotFound_WhenAddingSale_ThenThrowsNotFoundException()
        {
            var input = new AddSaleInput
            {
                SellerId = _faker.Random.Guid(),
                Products = new List<SaleProductInput>
                {
                    new SaleProductInput { ProductId = _faker.Random.Guid(), Quantity = 1 }
                }
            };

            _sellerRepositoryMock
                .Setup(repo => repo.GetByIdAsync(input.SellerId))
                .ReturnsAsync((Seller)null);

            await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(input, CancellationToken.None));
        }

        [Fact]
        public async Task GivenValidSale_WhenAddingSale_ThenAddsSaleSuccessfully()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var product = new Product("Test Product","TEST", 10.0m, 100, ProductCategoryEnum.Grocery);

            var input = new AddSaleInput
            {
                SellerId = seller.Id,
                Products = new List<SaleProductInput>
                {
                    new SaleProductInput { ProductId = product.Id, Quantity = 1 }
                }
            };


            _sellerRepositoryMock
                .Setup(repo => repo.GetByIdAsync(seller.Id))
                .ReturnsAsync(seller);

            _productRepositoryMock
                .Setup(repo => repo.GetByIdListAsync(It.IsAny<List<Guid>>()))
                .ReturnsAsync(new List<Product> { product });

            _saleRepositoryMock
                .Setup(repo => repo.AddAsync(It.IsAny<Sale>()))
                .Returns(Task.CompletedTask);

            var result = await _handler.Handle(input, CancellationToken.None);

            Assert.True(result.Success);
            Assert.Equal("The sale was registered successfully.", result.Message);
            Assert.NotNull(result.Data);
        }    
    }
}