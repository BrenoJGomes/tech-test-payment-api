﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Handlers.Sales;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sales
{
    public class GetAllSalesHandlerTests
    {
        private readonly Mock<ISaleRepository> _saleRepositoryMock;
        private readonly GetAllSalesHandler _handler;
        private readonly Faker _faker;

        public GetAllSalesHandlerTests()
        {
            _saleRepositoryMock = new Mock<ISaleRepository>();
            _handler = new GetAllSalesHandler(_saleRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSaleExist_WhenHandleIsCalled_ThenReturnsSales()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var products = new List<Sale>
                {
                    new Sale(seller),
                    new Sale(seller)
                };
            _saleRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(products);
            var request = new GetAllSalesInput();

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The sales were found successfully.", result.Message);
            Assert.Equal(products.Count, result.Data.Count());
        }

        [Fact]
        public async Task GivenNoSaleExist_WhenHandleIsCalled_ThenThrowsNotFoundException()
        {
            // Arrange
            _saleRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Sale>());
            var request = new GetAllSalesInput();

            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(request, CancellationToken.None));
            Assert.Equal("No sales were found.", exception.Message);
        }
    }
}
