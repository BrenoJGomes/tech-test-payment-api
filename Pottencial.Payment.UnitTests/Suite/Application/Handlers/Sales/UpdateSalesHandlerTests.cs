﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sales;
using Pottencial.Payment.Application.Handlers.Sales;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sales
{
    public class UpdateSalesHandlerTests
    {
        private readonly Mock<ISaleRepository> _saleRepositoryMock;
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly UpdateSaleProductsHandler _handler;
        private readonly Faker _faker;

        public UpdateSalesHandlerTests()
        {
            _saleRepositoryMock = new Mock<ISaleRepository>();
            _productRepositoryMock = new Mock<IProductRepository>();
            _handler = new UpdateSaleProductsHandler(_saleRepositoryMock.Object, _productRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSaleNotFound_WhenAddingSale_ThenThrowsNotFoundException()
        {
            // Arrange
            var saleId = Guid.NewGuid();
            _saleRepositoryMock.Setup(repo => repo.GetByIdAsync(saleId)).ReturnsAsync((Sale)null);
            var request = new UpdateSaleProductsInput { SaleId = saleId };

            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(request, CancellationToken.None));
            Assert.Equal("No sales were found.", exception.Message);
        }
    }
}