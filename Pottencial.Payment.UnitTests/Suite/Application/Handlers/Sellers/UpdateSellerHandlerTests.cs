﻿using Bogus;
using Bogus.Extensions.Brazil;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Handlers.Sellers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sellers
{
    public class UpdateSellerHandlerTests
    {
        private readonly Mock<ISellerRepository> _sellerRepositoryMock;
        private readonly UpdateSellerHandler _handler;
        private readonly Faker _faker;

        public UpdateSellerHandlerTests()
        {
            _sellerRepositoryMock = new Mock<ISellerRepository>();
            _handler = new UpdateSellerHandler(_sellerRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task Given_SellerExists_When_UpdatingSeller_Then_UpdatesSellerSuccessfully()
        {
            // Arrange
            var existingSeller = new Seller(_faker.Person.FullName, _faker.Person.Cpf(false), _faker.Person.Email, _faker.Person.Phone);

            var updateInput = new UpdateSellerInput
            {
                Id = existingSeller.Id,
                Name = _faker.Person.FullName,
                Cpf = _faker.Person.Cpf(false),
                Email = _faker.Person.Email,
                Phone = _faker.Person.Phone,
            };



            _sellerRepositoryMock.Setup(repo => repo.GetByIdAsync(existingSeller.Id)).ReturnsAsync(existingSeller);
            _sellerRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Seller>())).Returns(Task.CompletedTask);

            // Act
            var result = await _handler.Handle(updateInput, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The seller was update successfully.", result.Message);
            Assert.Equal(updateInput.Name, result.Data.Name);
            _sellerRepositoryMock.Verify(repo => repo.UpdateAsync(It.IsAny<Seller>()), Times.Once);
        }

        [Fact]
        public async Task Given_SellerDoesNotExist_When_UpdatingSeller_Then_ThrowsNotFoundException()
        {
            // Arrange
            var sellerId = Guid.NewGuid();
            var updateInput = new UpdateSellerInput { Id = sellerId };

            _sellerRepositoryMock.Setup(repo => repo.GetByIdAsync(sellerId)).ReturnsAsync((Seller)null);

            // Act & Assert
            await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(updateInput, CancellationToken.None));
        }
    }
}
