﻿using Bogus;
using Bogus.Extensions.Brazil;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Handlers.Sellers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sellers
{
    public class GetAllSellerHandlerTests
    {
        private readonly Mock<ISellerRepository> _sellerRepositoryMock;
        private readonly GetAllSellersHandler _handler;
        private readonly Faker _faker;

        public GetAllSellerHandlerTests()
        {
            _sellerRepositoryMock = new Mock<ISellerRepository>();
            _handler = new GetAllSellersHandler(_sellerRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSellersExist_WhenHandleIsCalled_ThenReturnsSellers()
        {
            var name = _faker.Commerce.ProductName();
            var cpf = _faker.Person.Cpf(false);
            var email = _faker.Person.Email;
            var phone = _faker.Person.Phone;

            var sellers = new List<Seller>
                {
                    new Seller(name, cpf, email, phone),
                    new Seller(name, cpf, email, phone)
                };
            _sellerRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(sellers);
            var request = new GetAllSellersInput();

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The sellers were found successfully.", result.Message);
            Assert.Equal(sellers.Count, result.Data.Count());
        }

        [Fact]
        public async Task GivenNoSellersExist_WhenHandleIsCalled_ThenThrowsNotFoundException()
        {
            // Arrange
            _sellerRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Seller>());
            var request = new GetAllSellersInput();

            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(request, CancellationToken.None));
            Assert.Equal("No seller were found.", exception.Message);
        }
    }
}
