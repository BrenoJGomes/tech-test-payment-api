﻿using Bogus;
using Bogus.Extensions.Brazil;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Handlers.Sellers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;
using System.Linq.Expressions;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sellers
{
    public class AddSellersHandlerTests
    {
        private readonly Mock<ISellerRepository> _sellerRepositoryMock;
        private readonly AddSellerHandler _handler;
        private readonly Faker _faker;

        public AddSellersHandlerTests()
        {
            _sellerRepositoryMock = new Mock<ISellerRepository>();
            _handler = new AddSellerHandler(_sellerRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSellerAlreadyExists_WhenAddingSeller_ThenThrowsDomainException()
        {
            var input = new AddSellerInput
            {
                Name = _faker.Person.FullName,
                Cpf = _faker.Person.Cpf(false),
                Email =  _faker.Person.Email,
                Phone = _faker.Person.Phone
            };

            _sellerRepositoryMock
                .Setup(repo => repo.ExistsAsync(It.IsAny<Expression<Func<Seller, bool>>>()))
                .ReturnsAsync(true);

            await Assert.ThrowsAsync<DomainException>(() => _handler.Handle(input, CancellationToken.None));
        }

        [Fact]
        public async Task GivenValidSeller_WhenAddingSeller_ThenAddsSellerSuccessfully()
        {
            var input = new AddSellerInput
            {
                Name = _faker.Person.FullName,
                Cpf = _faker.Person.Cpf(false),
                Email = _faker.Person.Email,
                Phone = _faker.Person.Phone
            };

            _sellerRepositoryMock
                .Setup(repo => repo.ExistsAsync(It.IsAny<Expression<Func<Seller, bool>>>()))
                .ReturnsAsync(false);

            _sellerRepositoryMock
                .Setup(repo => repo.AddAsync(It.IsAny<Seller>()))
                .Returns(Task.CompletedTask);

            var result = await _handler.Handle(input, CancellationToken.None);

            Assert.True(result.Success);
            Assert.Equal("The seller was registered successfully.", result.Message);
            Assert.NotNull(result.Data);
            Assert.Equal(input.Name, result.Data.Name);
        }
    }
}