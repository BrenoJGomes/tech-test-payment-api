﻿using Bogus;
using Bogus.Extensions.Brazil;
using FluentAssertions;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Handlers.Sellers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sellers
{
    public class DeleteSellerHandlerTests
    {
        private readonly Mock<ISellerRepository> _sellerRepositoryMock;
        private readonly DeleteSellerHandler _handler;
        private readonly Faker _faker;

        public DeleteSellerHandlerTests()
        {
            _sellerRepositoryMock = new Mock<ISellerRepository>();
            _handler = new DeleteSellerHandler(_sellerRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSellerExists_WhenDeleteSeller_ThenDeletesSeller()
        {
            // Arrange
            var name = _faker.Commerce.ProductName();
            var cpf = _faker.Person.Cpf(false);
            var email =_faker.Person.Email;
            var phone = _faker.Person.Phone;

            var seller = new Seller(name, cpf, email, phone);
            var request = new DeleteSellerInput { Id = seller.Id };

            _sellerRepositoryMock
                .Setup(repo => repo.GetByIdAsync(seller.Id))
                .ReturnsAsync(seller);

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            _sellerRepositoryMock.Verify(repo => repo.Delete(seller), Times.Once);
            Assert.True(result.Success);
            Assert.Equal("The seller was deleted successfully.", result.Message);
        }

        [Fact]
        public async Task GivenSellerDoesNotExist_WhenDeleteSeller_ThenThrowsException()
        {
            // Arrange
            var SellerId = Guid.NewGuid();
            var request = new DeleteSellerInput { Id = SellerId };
            _sellerRepositoryMock
                .Setup(repo => repo.GetByIdAsync(SellerId))
                .ReturnsAsync((Seller)null);

            // Act
            Func<Task> act = async () => await _handler.Handle(request, CancellationToken.None);

            // Assert
            await act.Should().ThrowAsync<NotFoundException>()
                .WithMessage("The seller does not exist.");
        }
    }
}