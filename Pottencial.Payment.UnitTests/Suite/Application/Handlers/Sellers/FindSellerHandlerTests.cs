﻿using Bogus;
using Bogus.Extensions.Brazil;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Commands.Inputs.Sellers;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Application.Handlers.Sellers;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Sellers
{
    public class FindSellerHandlerTests
    {
        private readonly Mock<ISellerRepository> _sellerRepositoryMock;
        private readonly FindSellerHandler _handler;
        private readonly Faker _faker;

        public FindSellerHandlerTests()
        {
            _sellerRepositoryMock = new Mock<ISellerRepository>();
            _handler = new FindSellerHandler(_sellerRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenSellerExists_WhenHandleCalled_ThenReturnsSellerViewModel()
        {
            var name = _faker.Commerce.ProductName();
            var cpf = _faker.Person.Cpf(false);
            var email = _faker.Person.Email;
            var phone = _faker.Person.Phone;

            var seller = new Seller(name, cpf, email, phone);
            _sellerRepositoryMock.Setup(repo => repo.GetByIdAsync(seller.Id)).ReturnsAsync(seller);
            var request = new FindSellerInput { Id = seller.Id };

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The seller was found successfully.", result.Message);
            Assert.NotNull(result.Data);
        }

        [Fact]
        public async Task GivenSellerDoesNotExist_WhenHandleCalled_ThenThrowsDomainException()
        {
            // Arrange
            var sellerId = Guid.NewGuid();
            _sellerRepositoryMock.Setup(repo => repo.GetByIdAsync(sellerId)).ReturnsAsync((Seller)null);
            var request = new FindSellerInput { Id = sellerId };

            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(request, CancellationToken.None));
            Assert.Equal("The seller was not found.", exception.Message);
        }
    }
}
