﻿using Bogus;
using FluentAssertions;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Products
{
    public class DeleteProductHandlerTests
    {
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly DeleteProductHandler _handler;
        private readonly Faker _faker;

        public DeleteProductHandlerTests()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _handler = new DeleteProductHandler(_productRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenProductExists_WhenDeleteProduct_ThenDeletesProduct()
        {

            var name = _faker.Commerce.ProductName();
            var description = _faker.Commerce.ProductDescription();
            var price = decimal.Parse(_faker.Commerce.Price());
            var stockQuantity = _faker.Random.Int(1, 100);

            var product = new Product(name, description, price, stockQuantity, ProductCategoryEnum.Books);
            var request = new DeleteProductInput { Id = product.Id };

            _productRepositoryMock
                .Setup(repo => repo.GetByIdAsync(product.Id))
                .ReturnsAsync(product);

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            _productRepositoryMock.Verify(repo => repo.Delete(product), Times.Once);
            Assert.True(result.Success);
            Assert.Equal("The product was deleted successfully.", result.Message);
        }

        [Fact]
        public async Task GivenProductDoesNotExist_WhenDeleteProduct_ThenThrowsException()
        {
            // Arrange
            var productId = Guid.NewGuid();
            var request = new DeleteProductInput { Id = productId };
            _productRepositoryMock
                .Setup(repo => repo.GetByIdAsync(productId))
                .ReturnsAsync((Product)null);

            // Act
            Func<Task> act = async () => await _handler.Handle(request, CancellationToken.None);

            // Assert
            await act.Should().ThrowAsync<NotFoundException>()
                .WithMessage("The product does not exist.");
        }
    }
}