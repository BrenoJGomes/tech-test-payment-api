﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Products
{
    public class FindProductHandlerTests
    {
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly FindProductHandler _handler;
        private readonly Faker _faker;

        public FindProductHandlerTests()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _handler = new FindProductHandler(_productRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenProductExists_WhenHandleCalled_ThenReturnsProductViewModel()
        {
            var name = _faker.Commerce.ProductName();
            var description = _faker.Commerce.ProductDescription();
            var price = decimal.Parse(_faker.Commerce.Price());
            var stockQuantity = _faker.Random.Int(1, 100);

            var product = new Product(name, description, price, stockQuantity, ProductCategoryEnum.Books);
            _productRepositoryMock.Setup(repo => repo.GetByIdAsync(product.Id)).ReturnsAsync(product);
            var request = new FindProductInput { Id = product.Id };

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The product was found successfully.", result.Message);
            Assert.NotNull(result.Data);
        }

        [Fact]
        public async Task GivenProductDoesNotExist_WhenHandleCalled_ThenThrowsDomainException()
        {
            // Arrange
            var productId = Guid.NewGuid();
            _productRepositoryMock.Setup(repo => repo.GetByIdAsync(productId)).ReturnsAsync((Product)null);
            var request = new FindProductInput { Id = productId };

            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(request, CancellationToken.None));
            Assert.Equal("The product was not found.", exception.Message);
        }
    }
}
