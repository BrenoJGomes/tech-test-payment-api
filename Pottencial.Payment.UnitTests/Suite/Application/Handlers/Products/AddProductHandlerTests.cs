﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;
using System.Linq.Expressions;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Products
{
    public class AddProductHandlerTests
    {
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly AddProductHandler _handler;
        private readonly Faker _faker;

        public AddProductHandlerTests()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _handler = new AddProductHandler(_productRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenProductAlreadyExists_WhenAddingProduct_ThenThrowsDomainException()
        {
            var input = new AddProductInput
            {
                Name = _faker.Commerce.ProductName(),
                Description = _faker.Commerce.ProductDescription(),
                Price =  decimal.Parse(_faker.Commerce.Price()),
                StockQuantity = _faker.Random.Int(1, 100),
                Category = ProductCategoryEnum.Books
            };

            _productRepositoryMock
                .Setup(repo => repo.ExistsAsync(It.IsAny<Expression<Func<Product, bool>>>()))
                .ReturnsAsync(true);

            await Assert.ThrowsAsync<DomainException>(() => _handler.Handle(input, CancellationToken.None));
        }

        [Fact]
        public async Task GivenValidProduct_WhenAddingProduct_ThenAddsProductSuccessfully()
        {
            var input = new AddProductInput
            {
                Name = _faker.Commerce.ProductName(),
                Description = _faker.Commerce.ProductDescription(),
                Price = decimal.Parse(_faker.Commerce.Price()),
                StockQuantity = _faker.Random.Int(1, 100),
                Category = ProductCategoryEnum.Books
            };

            _productRepositoryMock
                .Setup(repo => repo.ExistsAsync(It.IsAny<Expression<Func<Product, bool>>>()))
                .ReturnsAsync(false);

            _productRepositoryMock
                .Setup(repo => repo.AddAsync(It.IsAny<Product>()))
                .Returns(Task.CompletedTask);

            var result = await _handler.Handle(input, CancellationToken.None);

            Assert.True(result.Success);
            Assert.Equal("The product was registered successfully.", result.Message);
            Assert.NotNull(result.Data);
            Assert.Equal(input.Name, result.Data.Name);
        }
    }
}