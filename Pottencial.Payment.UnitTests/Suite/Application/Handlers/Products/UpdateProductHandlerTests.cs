﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Products
{
    public class UpdateProductHandlerTests
    {
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly UpdateProductHandler _handler;
        private readonly Faker _faker;

        public UpdateProductHandlerTests()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _handler = new UpdateProductHandler(_productRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task Given_ProductExists_When_UpdatingProduct_Then_UpdatesProductSuccessfully()
        {
            // Arrange
            var existingProduct = new Product(_faker.Commerce.ProductName(), _faker.Commerce.ProductDescription(), decimal.Parse(_faker.Commerce.Price(10.00m, 100.00m)), _faker.Random.Int(1, 100), ProductCategoryEnum.Books);

            var updateInput = new UpdateProductInput
            {
                Id = existingProduct.Id,
                Name = _faker.Commerce.ProductName(),
                Description = _faker.Commerce.ProductDescription(),
                Price = decimal.Parse(_faker.Commerce.Price(10.00m, 100.00m)),
                StockQuantity = _faker.Random.Int(1, 100),
                Category = ProductCategoryEnum.Books
            };



            _productRepositoryMock.Setup(repo => repo.GetByIdAsync(existingProduct.Id)).ReturnsAsync(existingProduct);
            _productRepositoryMock.Setup(repo => repo.UpdateAsync(It.IsAny<Product>())).Returns(Task.CompletedTask);

            // Act
            var result = await _handler.Handle(updateInput, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The product was update successfully.", result.Message);
            Assert.Equal(updateInput.Name, result.Data.Name);
            _productRepositoryMock.Verify(repo => repo.UpdateAsync(It.IsAny<Product>()), Times.Once);
        }

        [Fact]
        public async Task Given_ProductDoesNotExist_When_UpdatingProduct_Then_ThrowsNotFoundException()
        {
            // Arrange
            var productId = Guid.NewGuid();
            var updateInput = new UpdateProductInput { Id = productId };

            _productRepositoryMock.Setup(repo => repo.GetByIdAsync(productId)).ReturnsAsync((Product)null);

            // Act & Assert
            await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(updateInput, CancellationToken.None));
        }
    }
}
