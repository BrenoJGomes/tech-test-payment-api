﻿using Bogus;
using Moq;
using Pottencial.Payment.Application.Commands.Inputs.Products;
using Pottencial.Payment.Application.Handlers.Products;
using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using Pottencial.Payment.Domain.Interfaces.Repositories;

namespace Pottencial.Payment.UnitTests.Suite.Application.Handlers.Products
{
    public class GetAllProductsHandlerTests
    {
        private readonly Mock<IProductRepository> _productRepositoryMock;
        private readonly GetAllProductsHandler _handler;
        private readonly Faker _faker;

        public GetAllProductsHandlerTests()
        {
            _productRepositoryMock = new Mock<IProductRepository>();
            _handler = new GetAllProductsHandler(_productRepositoryMock.Object);
            _faker = new Faker();
        }

        [Fact]
        public async Task GivenProductsExist_WhenHandleIsCalled_ThenReturnsProducts()
        {
            var name = _faker.Commerce.ProductName();
            var description = _faker.Commerce.ProductDescription();
            var price = decimal.Parse(_faker.Commerce.Price());
            var stockQuantity = _faker.Random.Int(1, 100);

            var products = new List<Product>
                {
                    new Product(name, description, price, stockQuantity, ProductCategoryEnum.Books),
                    new Product(name, description, price, stockQuantity, ProductCategoryEnum.Books)
                };
            _productRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(products);
            var request = new GetAllProductsInput();

            // Act
            var result = await _handler.Handle(request, CancellationToken.None);

            // Assert
            Assert.True(result.Success);
            Assert.Equal("The products were found successfully.", result.Message);
            Assert.Equal(products.Count, result.Data.Count());
        }

        [Fact]
        public async Task GivenNoProductsExist_WhenHandleIsCalled_ThenThrowsNotFoundException()
        {
            // Arrange
            _productRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Product>());
            var request = new GetAllProductsInput();

            // Act & Assert
            var exception = await Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(request, CancellationToken.None));
            Assert.Equal("No products were found.", exception.Message);
        }
    }
}
