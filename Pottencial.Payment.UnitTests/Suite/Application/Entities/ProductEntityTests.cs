﻿using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;

namespace Pottencial.Payment.UnitTests.Suite.Application.Entities
{
    public class ProductEntityTests
    {
        [Fact]
        public void Given_Valid_Product_Initialization_When_Creating_Product_Then_Properties_Should_Set_Correctly()
        {
            var product = new Product("Test Product", "Test Description", 99.99m, 10, ProductCategoryEnum.Electronics);

            Assert.Equal("Test Product", product.Name);
            Assert.Equal("Test Description", product.Description);
            Assert.Equal(99.99m, product.Price);
            Assert.Equal(10, product.StockQuantity);
            Assert.Equal(ProductCategoryEnum.Electronics, product.Category);
        }

        [Fact]
        public void Given_Existing_Product_When_Updating_Product_Then_Properties_Should_Update_Correctly()
        {
            var product = new Product("Test Product", "Test Description", 99.99m, 10, ProductCategoryEnum.Electronics);
            product.Update("Updated Product", "Updated Description", 79.99m, 5, ProductCategoryEnum.Sports);

            Assert.Equal("Updated Product", product.Name);
            Assert.Equal("Updated Description", product.Description);
            Assert.Equal(79.99m, product.Price);
            Assert.Equal(5, product.StockQuantity);
            Assert.Equal(ProductCategoryEnum.Sports, product.Category);
        }
    }
}
