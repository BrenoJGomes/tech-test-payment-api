﻿using Pottencial.Payment.Domain.Entities;
using Pottencial.Payment.Domain.Enumerators;
using Pottencial.Payment.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pottencial.Payment.UnitTests.Suite.Application.Entities
{
    public class SaleEntityTests
    {
        [Fact]
        public void Given_Sale_When_Initialized_Then_Properties_Are_Set_Correctly()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var sale = new Sale(seller);

            Assert.Equal(seller.Id, sale.SellerId);
            Assert.Equal(seller, sale.Seller);
            Assert.Equal(SaleStatusEnum.AwaitingPayment, sale.Status);
            Assert.NotNull(sale.Products);
            Assert.Empty(sale.Products);
            Assert.True(sale.SaleDate <= DateTime.Now);
        }

        [Fact]
        public void Given_Null_Products_When_UpdateProducts_Called_Then_Throws_ArgumentException()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var sale = new Sale(seller);

            Assert.Throws<ArgumentException>(() => sale.UpdateProducts(null));
        }

        [Fact]
        public void Given_Products_When_UpdateProducts_Called_Then_Products_Are_Updated_And_TotalAmount_Calculated()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var sale = new Sale(seller);
            var products = new List<SaleProduct>
                {
                    new SaleProduct(),
                    new SaleProduct()
                };

            sale.UpdateProducts(products);

            Assert.Equal(2, sale.Products.Count);
            Assert.Equal(0, sale.TotalAmount);
        }

        [Fact]
        public void Given_PaymentApproved_Status_When_UpdateStatus_Called_Then_Throws_DomainException_On_Invalid_Transition()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var sale = new Sale(seller);
            sale.UpdateStatus(SaleStatusEnum.PaymentApproved);

            Assert.Throws<DomainException>(() => sale.UpdateStatus(SaleStatusEnum.AwaitingPayment));
        }

        [Fact]
        public void Given_Valid_Transition_When_UpdateStatus_Called_Then_Status_Is_Updated()
        {
            var seller = new Seller("Test Seller", "1234567912", "ëmail@email.com", "99412574");
            var sale = new Sale(seller);
            sale.UpdateStatus(SaleStatusEnum.PaymentApproved);

            Assert.Equal(SaleStatusEnum.PaymentApproved, sale.Status);
        }
    }
}
