﻿using Bogus;
using Pottencial.Payment.Domain.Entities;

namespace Pottencial.Payment.UnitTests.Suite.Application.Entities
{
    public class SellerEntityTests
    {
        private readonly Faker _faker = new Faker();

        [Fact]
        public void GivenValidSellerData_WhenConstructed_ThenPropertiesShouldBeInitialized()
        {
            // Arrange
            var name = _faker.Name.FullName();
            var cpf = _faker.Random.Replace("###.###.###-##");
            var email = _faker.Internet.Email();
            var phone = _faker.Phone.PhoneNumber();

            // Act
            var seller = new Seller(name, cpf, email, phone);

            // Assert
            Assert.Equal(name, seller.Name);
            Assert.Equal(cpf, seller.Cpf);
            Assert.Equal(email, seller.Email);
            Assert.Equal(phone, seller.Phone);
        }

        [Fact]
        public void GivenUpdatedSellerData_WhenUpdated_ThenPropertiesShouldChange()
        {
            // Arrange
            var seller = new Seller(_faker.Name.FullName(), _faker.Random.Replace("###.###.###-##"), _faker.Internet.Email(), _faker.Phone.PhoneNumber());
            var newName = _faker.Name.FullName();
            var newCpf = _faker.Random.Replace("###.###.###-##");
            var newEmail = _faker.Internet.Email();
            var newPhone = _faker.Phone.PhoneNumber();

            // Act
            seller.Update(newName, newCpf, newEmail, newPhone);

            // Assert
            Assert.Equal(newName, seller.Name);
            Assert.Equal(newCpf, seller.Cpf);
            Assert.Equal(newEmail, seller.Email);
            Assert.Equal(newPhone, seller.Phone);
        }
    }
}
